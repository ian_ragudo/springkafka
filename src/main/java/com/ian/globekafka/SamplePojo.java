package com.ian.globekafka;

public class SamplePojo {

    private String name;
    private String msisdn;

    public SamplePojo(String name, String msisdn) {
        this.name = name;
        this.msisdn = msisdn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    @Override
    public String toString() {
        return "SamplePojo{" +
                "name='" + name + '\'' +
                ", msisdn='" + msisdn + '\'' +
                '}';
    }
}
