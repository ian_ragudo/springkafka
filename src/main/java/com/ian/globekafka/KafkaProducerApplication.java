package com.ian.globekafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class KafkaProducerApplication {

	public static void main(String[] args) {

		ConfigurableApplicationContext context = SpringApplication.run(KafkaProducerApplication.class, args);

		KafkaProducer producer = context.getBean(KafkaProducer.class);

		producer.sendMessage("testTopic", "Hello, Kafka test");

		producer.sendPojoMessage("testTopic", new SamplePojo("Ian", "09171"));

	}

}
