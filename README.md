# README #

### What is this repository for? ###

This is a simple spring kafka producer that publishes messages to the specified topic

### How to use? ###

Sample calls in `KafkaProducerApplication` can be seen wherein a POJO, and a string were passed.

### Limitations ###

Since the goal is just to produce/publish messages and other downstream systems will do the consuming, the consumer part was not added.